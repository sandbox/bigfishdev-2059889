<?php
/**
 * @file
 * BIG FISH Payment Gateway wrapper class.
 */

use \BigFish\PaymentGateway;
use \BigFish\PaymentGateway\Config;
use \BigFish\PaymentGateway\Request\Init as InitRequest;
use \BigFish\PaymentGateway\Request\Start as StartRequest;
use \BigFish\PaymentGateway\Request\Result as ResultRequest;

/**
 * Class UcBigFishPaymentGateway.
 */
class UcBigFishPaymentGateway {

  const TABLE_NAME = 'uc_bigfish_paymentgateway_transactions';

  const PAYMENT_STATUS_INIT = 'init';
  const PAYMENT_STATUS_PENDING = 'pen';
  const PAYMENT_STATUS_SUCCESS = 'success';
  const PAYMENT_STATUS_FAILED = 'failed';

  protected $error = FALSE;
  protected $errorMsg;

  protected $warning = FALSE;
  protected $warningMsg;

  protected $order;
  protected $transactionId;
  protected $initResponse;
  protected $session;

  /**
   * UcBigFishPaymentGateway Class contructor.
   */
  public function __construct() {
    $arg = func_get_arg(0);

    if (!_uc_bigfish_paymentgateway_load_library()) {
      $this->setError();
      return;
    }

    $this->session =& _uc_bigfish_paymentgateway_get_session();

    if (is_object($arg)) {
      $order = $arg;

      if ((int) $order->order_id) {
        $this->order = $order;
      }

    }
    elseif (trim((string) $arg)) {

      $this->transactionId = $arg;

      $this->loadOrder($this->transactionId);

    }
    else {

      $this->setError();

    }

    $this->setGwConfig();
  }

  /**
   * Set the Payment Gateway SDK library config.
   */
  protected function setGwConfig() {
    $config = new Config();
    $form = uc_bigfish_paymentgateway_settings_form();

    foreach (element_children($form) as $key) {
      if (isset($form[$key]['#default_value'])) {
        $param = str_replace('uc_bigfish_paymentgateway_', '', $key);
        $param = str_replace('_', ' ', $param);
        $param = ucwords($param);
        $param = str_replace(' ', '', $param);
        $param[0] = strtolower($param[0]);

        $config->{$param} = $form[$key]['#default_value'];
      }
    }

    $module_info = system_get_info('module', 'uc_bigfish_paymentgateway');

    $config->moduleName = 'drupal_ubercart';
    $config->moduleVersion = $module_info['version'] ?: UC_BIGFISH_PAYMENTGATEWAY_VERSION;
    $config->testMode = (int) $config->testMode ? TRUE : FALSE;

    PaymentGateway::setConfig($config);
  }

  /**
   * Load Übercart order by transaction ID.
   *
   * @param string $transaction_id
   *   Transaction ID.
   *
   * @return bool
   *   order found and loaded
   */
  protected function loadOrder($transaction_id) {
    $order_id = db_select(self::TABLE_NAME, 't')
      ->fields('t', array('order_id'))
      ->condition('transaction_id', $transaction_id)
      ->execute()
      ->fetchField();

    if ((int) $order_id) {
      $order = uc_order_load($order_id);

      if (is_object($order)) {
        $this->order = $order;

        return TRUE;
      }
    }

    $this->setError(t('Invalid transaction ID!'));
    return FALSE;
  }

  /**
   * Return the loaded order object.
   *
   * @return object
   *   order object or false
   */
  public function getOrder() {
    return is_object($this->order) ? $this->order : FALSE;
  }

  /**
   * Initialize a new transaction for the loaded order.
   *
   * @return bool
   *   return false if initializing the transaction has failed
   */
  public function initTransaction() {
    global $language;

    $lang = $language->language;
    $order = $this->order;

    if (!is_object($order) || uc_order_status_data($order->order_status, 'state') !== 'in_checkout') {
      $_SESSION['cart_order'] = NULL;
      unset($_SESSION['cart_order']);
      drupal_goto('cart');

      return FALSE;
    }

    $provider_name = $this->session['provider_name'];
    $response_url = $GLOBALS['base_url'] . url('uc_bigfish_paymentgateway_result');
    $extra = $this->session;

    $request = new InitRequest();

    $request->setProviderName($provider_name)
      ->setResponseUrl($response_url)
      ->setAmount((float) $order->order_total)
      ->setCurrency($order->currency)
      ->setOrderId($order->order_id)
      ->setUserId($order->uid)
      ->setLanguage($lang);

    if (isset($extra['autoCommit']) && !$extra['autoCommit']) {
      $request->setAutoCommit(FALSE);
    }

    switch ($provider_name) {
      case PaymentGateway::PROVIDER_MKB_SZEP:
        $request->setMkbSzepCafeteriaId($extra['MkbSzepCafeteriaId']);
        $request->setGatewayPaymentPage(TRUE);
        break;

      case PaymentGateway::PROVIDER_KHB_SZEP:
        $request->setExtra(array(
          'KhbCardPocketId' => $extra['KhbCardPocketId'],
        ));
        break;

      case PaymentGateway::PROVIDER_WIRECARD_QPAY:
        if (isset($extra['QpayPaymentType']) && !empty($extra['QpayPaymentType'])) {
          $request->setExtra(array(
            'QpayPaymentType' => $extra['QpayPaymentType'],
          ));
        }
        break;

      case PaymentGateway::PROVIDER_OTP:
        if (!empty($extra['OtpCardPocketId'])) {
          $request->setOtpCardPocketId($extra['OtpCardPocketId']);
        }
        break;

      case PaymentGateway::PROVIDER_OTPAY:
        if (isset($extra['phoneNumber']) && !empty($extra['phoneNumber'])) {
          $request->setMppPhoneNumber($extra['phoneNumber']);
        }
    }

    $response = PaymentGateway::init($request);

    if ($response->ResultCode === PaymentGateway::RESULT_CODE_SUCCESS && $response->TransactionId) {
      $this->initResponse = $response;

      $transaction = array(
        'order_id'         => $order->order_id,
        'transaction_id'   => $response->TransactionId,
        'provider_name'    => $provider_name,
        'payment_total'    => $order->order_total,
        'payment_currency' => $order->currency,
        'language'         => $lang,
        'payment_status'   => self::PAYMENT_STATUS_INIT,
        'time_init'        => time(),
      );

      db_insert(self::TABLE_NAME)
        ->fields($transaction)
        ->execute();

      $this->startTransaction();
      return TRUE;
    }

    $error_message = $response->ResultCode . ': ' . $response->ResultMessage;
    $this->setError($error_message);
    return FALSE;
  }

  /**
   * Start the transaction and redirect the user, if necessary.
   *
   * @return bool
   *   return false if starting the transaction has failed
   */
  protected function startTransaction() {
    if (is_object($this->initResponse)) {
      $request = new StartRequest($this->initResponse->TransactionId);
      PaymentGateway::start($request);
    }

    return FALSE;
  }

  /**
   * Retrieve the transaction result of the loaded order.
   *
   * @return bool
   *   the result of the order finalization or false
   */
  public function processResult() {
    $transaction_id = $this->transactionId;
    $order = $this->order;

    if (!$transaction_id) {
      $this->setError(t('Invalid transaction ID!'));
      return FALSE;
    }

    if (!$order) {
      $this->setError(t('Order is not loaded!'));
      return FALSE;
    }

    $request = new ResultRequest($transaction_id);
    $response = PaymentGateway::result($request);

    $status_map = array(
      PaymentGateway::RESULT_CODE_SUCCESS     => self::PAYMENT_STATUS_SUCCESS,
      PaymentGateway::RESULT_CODE_PENDING     => self::PAYMENT_STATUS_PENDING,
      PaymentGateway::RESULT_CODE_OPEN        => self::PAYMENT_STATUS_PENDING,
      PaymentGateway::RESULT_CODE_ERROR       => self::PAYMENT_STATUS_FAILED,
      PaymentGateway::RESULT_CODE_USER_CANCEL => self::PAYMENT_STATUS_FAILED,
      PaymentGateway::RESULT_CODE_TIMEOUT     => self::PAYMENT_STATUS_FAILED,
    );
    $status = isset($status_map[$response->ResultCode]) ? $status_map[$response->ResultCode] : self::PAYMENT_STATUS_FAILED;

    db_update(self::TABLE_NAME)
      ->fields(array(
        'payment_status'          => $status,
        'payment_result_code'     => $response->ResultCode,
        'payment_result_message'  => $response->ResultMessage,
        'provider_transaction_id' => isset($response->ProviderTransactionId) ? $response->ProviderTransactionId : NULL,
        'payment_anum'            => isset($response->Anum) ? $response->Anum : NULL,
        'time_end_status'         => time(),
      ))
      ->condition('transaction_id', $transaction_id)
      ->execute();

    if ($status == self::PAYMENT_STATUS_SUCCESS) {

      return $this->finalizeOrder();

    }
    elseif ($status == self::PAYMENT_STATUS_PENDING) {

      $this->setWarning($response->ResultCode . ': ' . $response->ResultMessage);
      return $this->pendingOrder();

    }
    else {

      $error_message = $response->ResultCode . ': ' . $response->ResultMessage;
      $this->setError($error_message);

      return FALSE;
    }
  }

  /**
   * Finalize the order after a successful transaction.
   *
   * @return bool
   *   success
   */
  protected function finalizeOrder() {
    global $user;
    $order = $this->order;

    if ($order->order_status === 'payment_received') {
      return TRUE;
    }

    if ($success = uc_order_update_status($order->order_id, 'payment_received')) {
      uc_payment_enter($order->order_id, $order->payment_method, $order->order_total, $order->uid);

      $message = t('Order payment success.');

      uc_order_comment_save($order->order_id, $user->uid, $message, 'order', 'payment_received');
    }
    else {
      $this->setError(t('Order status update failed!'));
    }

    return $success;
  }

  /**
   * Set order pending status.
   *
   * @return bool
   *   success
   */
  protected function pendingOrder() {
    global $user;
    $order = $this->order;

    if ($order->order_status === 'paymentgateway_pending') {
      return TRUE;
    }

    if ($success = uc_order_update_status($order->order_id, 'paymentgateway_pending')) {
      $message = t('Order payment pending.');

      uc_order_comment_save($order->order_id, $user->uid, $message, 'order', 'paymentgateway_pending');
    }
    else {
      $this->setError(t('Order status update failed!'));
    }

    return $success;
  }

  /**
   * Set the internal error flag and message.
   *
   * @param string $msg
   *   The message.
   */
  protected function setError($msg = NULL) {
    $this->error = TRUE;
    $this->errorMsg = $msg;
  }

  /**
   * Return the internal error flag value.
   *
   * @return bool
   *   is error
   */
  public function getError() {
    return $this->error;
  }

  /**
   * Return the internal error message.
   *
   * @return string
   *   the message
   */
  public function getErrorMessage() {
    return $this->errorMsg;
  }

  /**
   * Set the internal warning flag and message.
   *
   * @param string $msg
   *   The message.
   */
  protected function setWarning($msg = NULL) {
    $this->warning = TRUE;
    $this->warningMsg = $msg;
  }

  /**
   * Return the internal warning flag value.
   *
   * @return bool
   *   is error
   */
  public function getWarning() {
    return $this->warning;
  }

  /**
   * Return the internal warning message.
   *
   * @return string
   *   the message
   */
  public function getWarningMessage() {
    return $this->warningMsg;
  }

}
