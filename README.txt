With BIG FISH Payment Gateway merchants can easily offer electronic payments
on their webshops.

Payment Gateway helps merchants to enable several payment services on their
websites in one step, without a time-consuming development, because the
technical communication between payment service providers and websites are
provided by the Payment Gateway and we provide an easy to use SDK to it.

Who is Payment Gateway for?

- Who wants to provide more than one type of electronic payment option on their
  website.
- Who wants real time feedback about their financial transactions.
- Who wants to have the possibility to change payment service provider quickly
  if it's needed.
- Who wants to do all of this without a huge development and testing process.

This module is produced for and works only with Übercart e-commerce module
version 7.x-3.x.

Installation:

- In order to use this module, you must download the gateway's SDK library
  within a ZIP file from this website:
  https://github.com/bigfish-hu/payment-gateway-php-sdk

- Extract the ZIP file content, and place it in one of the following library
  folders:
  - sites/all/libraries directory or
  - profiles/[yourprofilename]/libraries or
  - sites/example.com/libraries if you have a multi-site installation

- Configure the module global and per method settings on Übercart's Payment
  methods page (Store / Configuration / Payment methods)
