<?php
/**
 * @file
 * Install, update and uninstall functions for the module.
 */

/**
 * Implements hook_requirements().
 */
function uc_bigfish_paymentgateway_requirements($phase) {
  module_load_include('inc', 'uc_bigfish_paymentgateway');
  $requirements = array();
  $t = get_t();

  if ($phase === 'install') {
    if (!_uc_bigfish_paymentgateway_check_library()) {
      $requirements['uc_bigfish_paymentgateway_lib'] = array(
        'title' => $t('BIG FISH PaymentGateway SDK library'),
        'description' => $t('In order to use the BIG FISH Payment Gateway module, You must download the proper SDK library from <a href="@url" target="_blank">here</a>.', array(
          '@url' => 'http://www.paymentgateway.hu/technikai_informaciok.html',
        )),
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }
  elseif ($phase === 'runtime') {
    $has_curl = function_exists('curl_init');

    if (!$has_curl) {
      $requirements['uc_bigfish_paymentgateway_curl'] = array(
        'title' => $t('cURL'),
        'value' => $t('Not found'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t("BIG FISH PaymentGateway requires the PHP <a href='!curl_url'>cURL</a> library.", array('!curl_url' => 'http://php.net/manual/en/curl.setup.php')),
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function uc_bigfish_paymentgateway_schema() {
  $schema = array();

  $schema['uc_bigfish_paymentgateway_transactions'] = array(
    'description' => 'Logs PaymentGateway transactions',

    'fields' => array(
      'order_id' => array(
        'description' => 'The order ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'transaction_id' => array(
        'description' => 'The transaction ID from PaymentGateway.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'provider_name' => array(
        'description' => 'The selected provider ID.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'provider_transaction_id' => array(
        'description' => 'The transaction ID from the selected provider.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'payment_total' => array(
        'description' => 'The payment total.',
        'type' => 'float',
        'not null' => TRUE,
      ),
      'payment_currency' => array(
        'description' => 'The payment currency.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'language' => array(
        'description' => 'The selected language.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 'hu',
      ),
      'payment_status' => array(
        'description' => 'The transaction status from PaymentGateway.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'payment_result_code' => array(
        'description' => 'The result code from PaymentGateway.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'payment_result_message' => array(
        'description' => 'The result message from PaymentGateway.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'payment_anum' => array(
        'description' => 'The authorization number from the selected provider.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'time_init' => array(
        'description' => 'The date of the initialization.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'time_end_status' => array(
        'description' => 'The date of the end status.',
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
    ),
    'indexes' => array(
      'order_id' => array('order_id'),
      'transaction_id' => array('transaction_id'),
    ),
    'foreign keys' => array(
      'order_id' => array('uc_orders' => 'order_id'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function uc_bigfish_paymentgateway_install() {
  $t = get_t();

  db_merge('uc_order_statuses')
    ->key(array('order_status_id' => 'paymentgateway_pending'))
    ->insertFields(array(
      'order_status_id' => 'paymentgateway_pending',
      'title' => $t('PaymentGateway pending'),
      'state' => 'payment_received',
      'weight' => 7,
      'locked' => 1,
    ))
    ->updateFields(array(
      'state' => 'payment_received',
      'locked' => 1,
    ))
    ->execute();

  drupal_mkdir('public://uc_bigfish_paymentgateway');
}

/**
 * Implements hook_uninstall().
 */
function uc_bigfish_paymentgateway_uninstall() {
  db_update('uc_order_statuses')
    ->fields(array(
      'locked' => 0,
    ))
    ->condition('order_status_id', 'paymentgateway_pending')
    ->execute();

  $files = file_scan_directory('public://uc_bigfish_paymentgateway', '/.*/');

  if (is_array($files)) {
    foreach ($files as $file) {
      $provider_id = strtolower($file->name);
      $fid = variable_get('uc_bigfish_paymentgateway_' . $provider_id . '_logo', FALSE);

      if ($fid && ($file = file_load($fid))) {
        file_delete($file);
      }
    }
  }

  drupal_rmdir('public://uc_bigfish_paymentgateway');

  db_delete('variable')
    ->condition('name', 'uc_bigfish_paymentgateway_%', 'LIKE')
    ->execute();
}
