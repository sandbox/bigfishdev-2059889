<?php
/**
 * @file
 * BIG FISH Payment Gateway module.
 */

use \BigFish\PaymentGateway;

define('UC_BIGFISH_PAYMENTGATEWAY_VERSION', '7.x-3.x');
define('UC_BIGFISH_PAYMENTGATEWAY_DOWNLOAD_URL', 'https://github.com/bigfish-hu/payment-gateway-php-sdk');
define('UC_BIGFISH_PAYMENTGATEWAY_LIBRARY_NAME', 'payment-gateway-php-sdk-master');

/**
 * Returns the implemented payment methods.
 *
 * @return array
 *   array of payment method ID's and titles
 */
function _uc_bigfish_paymentgateway_methods() {
  module_load_include('inc', 'uc_bigfish_paymentgateway');

  if (!_uc_bigfish_paymentgateway_load_library()) {
    return array();
  }

  return array(
    PaymentGateway::PROVIDER_BORGUN           => t('Borgun'),
    PaymentGateway::PROVIDER_CIB              => t('Hungarian CIB Bank'),
    PaymentGateway::PROVIDER_ESCALION         => t('Escalion'),
    PaymentGateway::PROVIDER_FHB              => t('Hungarian FHB Bank'),
    PaymentGateway::PROVIDER_KHB              => t('Hungarian K&H Bank'),
    PaymentGateway::PROVIDER_KHB_SZEP         => t('Hungarian K&H Bank - "Szép" Card'),
    PaymentGateway::PROVIDER_MKB_SZEP         => t('Hungarian MKB Bank - "Szép" Card'),
    PaymentGateway::PROVIDER_OTP              => t('Hungarian OTP Bank'),
    PaymentGateway::PROVIDER_OTP_SIMPLE       => t('Hungarian OTP Bank - Simple'),
    PaymentGateway::PROVIDER_OTP_SIMPLE_WIRE  => t('Hungarian OTP Bank - Simple wire'),
    PaymentGateway::PROVIDER_OTPAY            => t('OTPay'),
    PaymentGateway::PROVIDER_OTPAY_MASTERPASS => t('OTPay MasterPass'),
    PaymentGateway::PROVIDER_OTP_MULTIPONT    => t('OTP Multipont'),
    PaymentGateway::PROVIDER_PAYPAL           => t('PayPal'),
    PaymentGateway::PROVIDER_PAYU2            => t('PayU'),
    PaymentGateway::PROVIDER_PAYSAFECARD      => t('Paysafecard'),
    PaymentGateway::PROVIDER_SAFERPAY         => t('Saferpay'),
    PaymentGateway::PROVIDER_SMS              => t('SMS'),
    PaymentGateway::PROVIDER_SOFORT           => t('SOFORT Banking'),
    PaymentGateway::PROVIDER_UNICREDIT        => t('UniCredit Bank'),
    PaymentGateway::PROVIDER_WIRECARD_QPAY    => t('Wirecard QPAY'),
  );
}

/**
 * Implements hook_init().
 */
function uc_bigfish_paymentgateway_init() {
  module_load_include('inc', 'uc_bigfish_paymentgateway');
  module_load_include('class.inc', 'uc_bigfish_paymentgateway');
}

/**
 * Implements hook_menu().
 */
function uc_bigfish_paymentgateway_menu() {
  $items['uc_bigfish_paymentgateway_redirect'] = array(
    'title' => 'Paymentgateway redirect page',
    'page callback' => 'uc_bigfish_paymentgateway_redirect',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['uc_bigfish_paymentgateway_result'] = array(
    'title' => 'Paymentgateway result page',
    'page callback' => 'uc_bigfish_paymentgateway_result',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $uc_payment_items = uc_payment_menu();
  $items['admin/store/settings/payment/uc_bigfish_paymentgateway_default'] = $uc_payment_items['admin/store/settings/payment'];
  $items['admin/store/settings/payment/uc_bigfish_paymentgateway_default']['type'] = MENU_DEFAULT_LOCAL_TASK;
  $items['admin/store/settings/payment/uc_bigfish_paymentgateway_default']['weight'] = 0;

  $items['admin/store/settings/payment/uc_bigfish_paymentgateway'] = array(
    'title' => 'BIG FISH Payment gateway',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_bigfish_paymentgateway_settings_form'),
    'access arguments' => array('administer store'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  $items['admin/store/orders/%uc_order/bf_pmgw_transactions'] = array(
    'title' => 'Transactions',
    'description' => 'Show payment transaction made against an order.',
    'page callback' => 'uc_bigfish_paymentgateway_order_transactions',
    'page arguments' => array(3),
    'access arguments' => array('view payments'),
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
    'file' => 'uc_bigfish_paymentgateway.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_libraries_info().
 */
function uc_bigfish_paymentgateway_libraries_info() {
  $libraries[UC_BIGFISH_PAYMENTGATEWAY_LIBRARY_NAME] = array(
    'name' => 'BIG FISH Payment Gateway - PHP SDK',
    'vendor url' => 'https://www.paymentgateway.hu',
    'download url' => 'https://github.com/bigfish-hu/payment-gateway-php-sdk/archive/master.zip',
    'version callback' => 'uc_bigfish_paymentgateway_library_get_version',
    'files' => array(
      'php' => array(
        'src/BigFish/PaymentGateway/Autoload.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Library version callback function.
 *
 * @return string
 *   library version
 */
function uc_bigfish_paymentgateway_library_get_version() {
  return 'master';
}

/**
 * Payment Gateway's Global Settings form.
 *
 * @return array
 *   form declaration
 */
function uc_bigfish_paymentgateway_settings_form() {
  $form = array();

  $form['uc_bigfish_paymentgateway_store_name'] = array(
    '#title' => t('Online Store Name'),
    '#description' => '',
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_bigfish_paymentgateway_store_name', ''),
    '#required' => TRUE,
  );

  $form['uc_bigfish_paymentgateway_api_key'] = array(
    '#title' => t('BIG FISH Payment Gateway API Key'),
    '#description' => '',
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_bigfish_paymentgateway_api_key', ''),
    '#required' => TRUE,
  );

  $form['uc_bigfish_paymentgateway_test_mode'] = array(
    '#title' => t('Test mode'),
    '#description' => '',
    '#type' => 'radios',
    '#default_value' => variable_get('uc_bigfish_paymentgateway_test_mode', 1),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'uc_bigfish_paymentgateway_settings_form_submit';

  return $form;
}

/**
 * Payment Gateway's global settings submit function.
 *
 * @param array $form
 *   Form data.
 * @param array $form_state
 *   Form state data.
 */
function uc_bigfish_paymentgateway_settings_form_submit(array $form, array &$form_state) {
  $form_state['redirect'] = 'admin/store/settings/payment';
  drupal_redirect_form($form_state);
}

/**
 * Implements hook_uc_store_status().
 */
function uc_bigfish_paymentgateway_uc_store_status() {
  $statuses = array();

  if (!_uc_bigfish_paymentgateway_settings_set()) {
    $statuses[] = array(
      'status' => 'error',
      'title' => t('BIG FISH PaymentGateway'),
      'desc' => t('BIG FISH PaymentGateway settings must be configured <a href="!url">here</a>.', array('!url' => url('admin/store/settings/payment/uc_bigfish_paymentgateway'))),
    );
  }

  return $statuses;
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_bigfish_paymentgateway_uc_payment_method() {

  $paymentgateway_methods = _uc_bigfish_paymentgateway_methods();
  $module_path = drupal_get_path('module', 'uc_bigfish_paymentgateway');

  $i = 1;
  foreach ($paymentgateway_methods as $provider_id => $provider_name) {
    $custom_name = trim(variable_get('uc_bigfish_paymentgateway_' . strtolower($provider_id) . '_title', FALSE));

    $methods[] = array(
      'id'         => 'paymentgateway_' . strtolower($provider_id),
      'name'       => 'BIG FISH PaymentGateway - ' . ($custom_name ? $custom_name . ' (' : '') . $provider_name . ($custom_name ? ')' : ''),
      'title'      => $custom_name ? $custom_name : $provider_name,
      'review'     => $custom_name ? $custom_name : $provider_name,
      'desc'       => t('Redirect users to submit payments through PaymentGateway.'),
      'callback'   => 'uc_bigfish_paymentgateway_method_' . strtolower($provider_id),
      'redirect'   => 'uc_bigfish_paymentgateway_redirect_form',
      'weight'     => $i + 10,
      'checkout'   => FALSE,
      'no_gateway' => TRUE,
    );
    $i++;

    require_once $module_path . '/methods/uc_bigfish_paymentgateway_' . strtolower($provider_id) . '.inc';
  }

  return $methods;
}

/**
 * Common Übercart callback for all the payment methods.
 *
 * @param string $op
 *   The operation being performed.
 * @param object $order
 *   The order object that relates to this operation.
 * @param array $form
 *   Where applicable, the form object that relates to this operation.
 * @param array $form_state
 *   Where applicable, the form state that relates to this operation.
 *
 * @staticvar type $methods
 *   The supported payment methods.
 * @staticvar type $providers
 *   Provider ID and name map.
 *
 * @return mixed
 *   Dependent on $op.
 */
function uc_bigfish_paymentgateway_method_common($op, &$order, $form, &$form_state, $caller) {
  static $methods, $providers;

  if (!$methods) {
    $methods = _uc_bigfish_paymentgateway_methods();
    foreach ($methods as $provider_id => $method) {
      $providers[strtolower($provider_id)] = $provider_id;
    }
  }

  $provider_id = str_replace('uc_bigfish_paymentgateway_method_', '', $caller);

  if (!in_array($provider_id, array_keys($providers))) {
    return FALSE;
  }

  switch ($op) {
    case 'settings':
      $form = array();

      $form['#submit'][] = 'uc_bigfish_paymentgateway_method_settings_submit';

      $form['uc_bigfish_paymentgateway_provider_id'] = array(
        '#type' => 'value',
        '#value' => $provider_id,
      );

      $form['uc_bigfish_paymentgateway_' . $provider_id . '_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Custom title'),
        '#description' => t('The human-readable title of the payment method, displayed during checkout.'),
        '#default_value' => variable_get('uc_bigfish_paymentgateway_' . $provider_id . '_title'),
      );

      $form['uc_bigfish_paymentgateway_' . $provider_id . '_logo'] = array(
        '#type' => 'managed_file',
        '#title' => t('Logo'),
        '#description' => t('The logo image of the payment method, displayed during checkout.'),
        '#default_value' => variable_get('uc_bigfish_paymentgateway_' . $provider_id . '_logo', NULL),
        '#process' => array(
          'uc_bigfish_paymentgateway_field_logo_process',
        ),
      );

      if (!_uc_bigfish_paymentgateway_check_library()) {
        drupal_set_message(t('In order use the BIG FISH Payment Gateway module, You must download the proper SDK library from <a href="@url" target="_blank">here</a>.', array(
          '@url' => UC_BIGFISH_PAYMENTGATEWAY_DOWNLOAD_URL,
        )), 'warning', FALSE);
      }

      if (!_uc_bigfish_paymentgateway_settings_set()) {
        drupal_set_message(t('BIG FISH PaymentGateway settings must be configured <a href="@url">here</a>.', array('@url' => url('admin/store/settings/payment/uc_bigfish_paymentgateway'))), 'warning', FALSE);
      }

      return $form;

    case 'cart-details':
      $method_title = variable_get('uc_bigfish_paymentgateway_' . $provider_id . '_title', $methods[$providers[$provider_id]]);
      $logo_fid = variable_get('uc_bigfish_paymentgateway_' . $provider_id . '_logo', NULL);

      if ($logo_fid && $logo_file = file_load($logo_fid)) {
        $logo_uri = $logo_file->uri;
      }
      else {
        $logo_uri = drupal_get_path('module', 'uc_bigfish_paymentgateway') . '/images/' . $provider_id . '.png';
      }
      $logo = '';

      if ($logo_uri) {
        $logo = theme('image', array(
          'path' => $logo_uri,
          'attributes' => array(
            'height' => 30,
            'style' => 'margin-right: 10px; vertical-align: middle;',
          ),
        ));
      }

      $form = array();

      $form['title'] = array(
        '#markup' => $logo . '<b>' . $method_title . '</b>',
      );

      return $form;

    case 'cart-process':
      $session =& _uc_bigfish_paymentgateway_get_session();
      $session['provider_name'] = $providers[$provider_id];
      return TRUE;

    case 'cart-review':
    case 'customer-view':
    case 'order-delete':
    case 'order-details':
    case 'order-process':
    case 'order-save':
    case 'order-submit':
    case 'order-view':
    case 'order-load':
      break;

    default:
      return FALSE;
  }

}

/**
 * Markup form element processor for the a payment method's logo.
 *
 * @param array $element
 *   The form element.
 * @param array $form_state
 *   Form state data.
 * @param array $form
 *   Form data.
 *
 * @return array
 *   processed element
 */
function uc_bigfish_paymentgateway_field_logo_process(array $element, array &$form_state, array $form) {
  $element = file_managed_file_process($element, $form_state, $form);

  if ($element['#file']) {
    $uri = $element['#file']->uri;
  }
  else {
    $uri = drupal_get_path('module', 'uc_bigfish_paymentgateway') . '/images/' . $form_state['values']['uc_bigfish_paymentgateway_provider_id'] . '.png';
  }

  $img = theme('image', array(
    'height' => 30,
    'path' => $uri,
  ));

  $element['preview'] = array(
    '#markup' => $img . '<br />',
    '#weight' => -20,
  );

  return $element;
}

/**
 * Payment method's settings form submit callback.
 *
 * @param array $form
 *   Form data.
 * @param array $form_state
 *   Form state data.
 */
function uc_bigfish_paymentgateway_method_settings_submit(array $form, array &$form_state) {
  $values = &$form_state['values'];

  $provider_id = $values['uc_bigfish_paymentgateway_provider_id'];

  if (!trim($values['uc_bigfish_paymentgateway_' . $provider_id . '_title'])) {
    unset($values['uc_bigfish_paymentgateway_' . $provider_id . '_title']);
    variable_del('uc_bigfish_paymentgateway_' . $provider_id . '_title');
  }

  if ($fid = $values['uc_bigfish_paymentgateway_' . $provider_id . '_logo']) {
    $file = file_load($fid);

    if (substr($file->uri, 0, 12) === 'temporary://') {
      $ext = pathinfo($file->uri, PATHINFO_EXTENSION);

      if (!file_move($file, 'public://uc_bigfish_paymentgateway/' . $provider_id . '.' . $ext, FILE_EXISTS_REPLACE)) {
        drupal_set_message(t('Can not move logo file!'), 'error');
      }
    }
  }
  else {
    unset($values['uc_bigfish_paymentgateway_' . $provider_id . '_logo']);
    variable_del('uc_bigfish_paymentgateway_' . $provider_id . '_logo');
  }
}

/**
 * Implements hook_uc_payment_gateway().
 */
function uc_bigfish_paymentgateway_uc_payment_gateway() {

  $gateways['paymentgateway'] = array(
    'title' => t('BIG FISH PaymentGateway'),
    'description' => t('BIG FISH PaymentGateway'),
  );

  return $gateways;
}

/**
 * Modify the order's review form action and redirect it to the Payment Gateway.
 *
 * @param array $form
 *   Form data.
 * @param array $form_state
 *   Form state data.
 * @param object $order
 *   The order in the checkout process.
 *
 * @return array
 *   the modified form data
 */
function uc_bigfish_paymentgateway_redirect_form(array $form, array $form_state, $order) {
  $time = time();
  $order_id = $order->order_id;
  $order_total = number_format($order->order_total, 2, '.', '');
  $customer_email = $order->primary_email;
  $cart_id = uc_cart_get_id();

  $data = array(
    'timestamp' => $time,
    'order_id' => $order_id,
    'order_total' => $order_total,
    'customer_email' => $customer_email,
    'cart_id' => $cart_id,
  );

  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array(
        '#type' => 'value',
        '#value' => $value,
      );
    }
  }

  $form['#action'] = url('uc_bigfish_paymentgateway_redirect');
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Pay order'),
    ),
  );

  return $form;
}

/**
 * Initialize and start the transaction, then redirects to the Payment Gateway.
 */
function uc_bigfish_paymentgateway_redirect() {
  $order_id = (int) $_SESSION['cart_order'];
  $order = uc_order_load($order_id);

  $gw = new UcBigFishPaymentGateway($order);

  if (!$gw->getError()) {
    // Redirect user and exit.
    $gw->initTransaction();
  }

  drupal_set_message($gw->getErrorMessage(), 'error');
  drupal_goto('cart');
}

/**
 * Menu callback after the return from the Payment Gateway.
 */
function uc_bigfish_paymentgateway_result() {
  $transaction_id = trim($_GET['TransactionId']);

  $uc_bigfish_paymentgateway = new UcBigFishPaymentGateway($transaction_id);

  if ($uc_bigfish_paymentgateway->processResult()) {
    $order = $uc_bigfish_paymentgateway->getOrder();

    unset($_SESSION['uc_checkout'][$order->order_id]['do_review']);
    $_SESSION['uc_checkout'][$order->order_id]['do_complete'] = TRUE;

    _uc_bigfish_paymentgateway_clear_session();

    if ($uc_bigfish_paymentgateway->getWarning()) {
      drupal_set_message($uc_bigfish_paymentgateway->getWarningMessage(), 'warning');
    }

    drupal_goto('cart/checkout/complete');
  }
  else {
    if ($uc_bigfish_paymentgateway->getError()) {
      drupal_set_message($uc_bigfish_paymentgateway->getErrorMessage(), 'error');
    }

    drupal_goto('cart');
  }
}

/**
 * Set and return the module's session data.
 *
 * @staticvar array $session
 *   internal session data
 *
 * @return array
 *   internal session data
 */
function &_uc_bigfish_paymentgateway_get_session() {
  static $session;

  if (!isset($session)) {
    $session = &$_SESSION['uc_bigfish_paymentgateway'];

    if (!is_array($session)) {
      $session = array();
    }
  }

  return $session;
}

/**
 * Clear module's session data.
 */
function _uc_bigfish_paymentgateway_clear_session() {
  $session = &_uc_bigfish_paymentgateway_get_session();
  $session = array();
}

/**
 * Checks the checkout process status. Used by the OTP Bank payment methods.
 *
 * @param array $form_state
 *   Form state data.
 *
 * @return bool
 *   the order review page is being loaded or not
 */
function _uc_bigfish_paymentgateway_cart_process_validate(array $form_state) {
  $last_button = $form_state['buttons'][count($form_state['buttons']) - 1];

  return $form_state['clicked_button']['#value'] === $last_button['#value'];
}

/**
 * Implements hook_action_info().
 */
function uc_bigfish_paymentgateway_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'uc_payment_methods_form') {
    // Fix weight delta default value (=10).
    foreach (element_children($form['pmtable']) as $method) {
      $element = &$form['pmtable'][$method];

      if (is_array($element)) {
        foreach ($element as &$field) {
          if (isset($field['#type']) && $field['#type'] === 'weight') {
            $field['#delta'] = isset($field['#delta']) ? max((int) $field['#delta'], 20) : 20;
          }
        }
      }
    }
  }
}
