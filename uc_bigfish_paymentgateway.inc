<?php

/**
 * @file
 * Module's common functions.
 */

use \BigFish\PaymentGateway\Autoload;

/**
 * Checks whether the required SDK library has been installed.
 *
 * @staticvar bool $result
 *
 * @return bool
 *   library installed
 */
function _uc_bigfish_paymentgateway_check_library() {
  static $library;

  if (!isset($library)) {
    $library = libraries_detect(UC_BIGFISH_PAYMENTGATEWAY_LIBRARY_NAME);
  }

  $found = is_array($library) && $library['installed'];

  if (!$found) {
    drupal_set_message($library['error message'], 'error', FALSE);
  }

  return $found;
}

/**
 * Load the required SDK library.
 *
 * @staticvar bool $loaded
 *
 * @return bool
 *   library loaded successfully
 */
function _uc_bigfish_paymentgateway_load_library() {
  static $loaded;

  if (isset($loaded)) {
    return $loaded;
  }

  if (_uc_bigfish_paymentgateway_check_library()) {
    libraries_load(UC_BIGFISH_PAYMENTGATEWAY_LIBRARY_NAME);
    Autoload::register();
    $loaded = TRUE;
  }
  else {
    $loaded = FALSE;
  }

  return $loaded;
}

/**
 * Checks whether module's variables have been set or not.
 *
 * @return bool
 *   the variables has been set
 */
function _uc_bigfish_paymentgateway_settings_set() {
  return variable_get('uc_bigfish_paymentgateway_store_name', FALSE) && variable_get('uc_bigfish_paymentgateway_api_key', FALSE);
}
