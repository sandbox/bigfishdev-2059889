<?php
/**
 * @file
 * BIG FISH Payment Gateway module / CIB payment method.
 */

/**
 * CIB payment method callback.
 */
function uc_bigfish_paymentgateway_method_cib($op, &$order, $form = NULL, &$form_state = NULL) {

  $result = uc_bigfish_paymentgateway_method_common($op, $order, $form, $form_state, __FUNCTION__);

  return $result;
}
