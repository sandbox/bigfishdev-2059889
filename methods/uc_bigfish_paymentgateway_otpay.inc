<?php
/**
 * @file
 * BIG FISH Payment Gateway module / OTPay payment method.
 */

/**
 * OTPay payment method callback.
 */
function uc_bigfish_paymentgateway_method_otpay($op, &$order, $form = NULL, &$form_state = NULL) {

  $result = uc_bigfish_paymentgateway_method_common($op, $order, $form, $form_state, __FUNCTION__);

  switch ($op) {
    case 'cart-details':
      $form = &$result;
      $session =& _uc_bigfish_paymentgateway_get_session();

      $form['uc_bigfish_paymentgateway_extra']['phoneNumber'] = array(
        '#type' => 'textfield',
        '#title' => t('Phone number'),
        '#default_value' => isset($session['phoneNumber']) ? $session['phoneNumber'] : NULL,
      );
      break;

    case 'cart-process':
      if (!_uc_bigfish_paymentgateway_cart_process_validate($form_state)) {
        return $result;
      }

      $session =& _uc_bigfish_paymentgateway_get_session();
      $values = &$form_state['values']['panes']['payment']['details'];

      $session['phoneNumber'] = trim($values['uc_bigfish_paymentgateway_extra']['phoneNumber']);
      break;

    case 'cart-review':
      $session =& _uc_bigfish_paymentgateway_get_session();
      $extra = array();

      if (isset($session['phoneNumber']) && trim($session['phoneNumber'])) {
        $extra[] = array(
          'title' => t('Phone number'),
          'data'  => $session['phoneNumber'],
        );
      }

      return $extra;
  }

  return $result;
}
