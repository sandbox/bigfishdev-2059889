<?php
/**
 * @file
 * BIG FISH Payment Gateway module / QPay payment method.
 */

/**
 * Wirecard QPay payment method callback.
 */
function uc_bigfish_paymentgateway_method_qpay($op, &$order, $form = NULL, &$form_state = NULL) {
  static $options;

  if (!isset($options)) {
    $options = array(
      'SELECT'                => t("I'll choose on Wirecard's website"),
      'BANCONTACT_MISTERCASH' => t('Bancontact/Mister Cash'),
      'CCARD'                 => t('Credit Card, Maestro SecureCode'),
      'CCARD-MOTO'            => t('Credit Card - Mail Order and Telephone Order'),
      'EKONTO'                => t('eKonto'),
      'EPAY_BG'               => t('ePay.bg'),
      'EPS'                   => t('eps Online-átutalás'),
      'GIROPAY'               => t('giropay'),
      'IDL'                   => t('iDEAL'),
      'MONETA'                => t('moneta.ru'),
      'MPASS'                 => t('mpass'),
      'PRZELEWY24'            => t('Przelewy24'),
      'PAYPAL'                => t('PayPal'),
      'PBX'                   => t('paybox'),
      'POLI'                  => t('POLi'),
      'PSC'                   => t('paysafecard'),
      'QUICK'                 => t('@Quick'),
      'SEPA-DD'               => t('SEPA Direct Debit'),
      'SKRILLDIRECT'          => t('Skrill Direct'),
      'SKRILLWALLET'          => t('Skrill Digital Wallet'),
      'SOFORTUEBERWEISUNG'    => t('SOFORT Banking'),
      'TATRAPAY'              => t('TatraPay'),
      'TRUSTLY'               => t('Trustly'),
      'TRUSTPAY'              => t('TrustPay'),
      'VOUCHER'               => t('My Voucher'),
    );
  }

  $result = uc_bigfish_paymentgateway_method_common($op, $order, $form, $form_state, __FUNCTION__);

  switch ($op) {
    case 'cart-details':
      $form = &$result;
      $session =& _uc_bigfish_paymentgateway_get_session();

      $form['uc_bigfish_paymentgateway_extra']['QpayPaymentType'] = array(
        '#type'          => 'select',
        '#title'         => t('Payment type'),
        '#options'       => $options,
        '#default_value' => isset($session['QpayPaymentType']) ? $session['QpayPaymentType'] : NULL,
      );
      break;

    case 'cart-process':
      if (!_uc_bigfish_paymentgateway_cart_process_validate($form_state)) {
        return $result;
      }

      $session =& _uc_bigfish_paymentgateway_get_session();
      $values = &$form_state['values']['panes']['payment']['details']['uc_bigfish_paymentgateway_extra'];

      $session['QpayPaymentType'] = $values['QpayPaymentType'];
      break;

    case 'cart-review':
      $session =& _uc_bigfish_paymentgateway_get_session();
      $extra = array();

      if (isset($session['QpayPaymentType']) && isset($options[$session['QpayPaymentType']])) {
        $extra = array(
          array(
            'title' => t('Payment type'),
            'data'  => $options[$session['QpayPaymentType']],
          ),
        );
      }

      return $extra;
  }

  return $result;
}
