<?php
/**
 * @file
 * BIG FISH Payment Gateway module / OTP Bank payment method.
 */

/**
 * OTP Bank payment method callback.
 */
function uc_bigfish_paymentgateway_method_otp($op, &$order, $form = NULL, &$form_state = NULL) {
  static $options;

  if (!isset($options)) {
    $options = array(
      '09' => t('Accomodation'),
      '07' => t('Hospitality'),
      '08' => t('Leisure'),
    );
  }

  $result = uc_bigfish_paymentgateway_method_common($op, $order, $form, $form_state, __FUNCTION__);

  switch ($op) {
    case 'cart-details':
      $form = &$result;
      $session =& _uc_bigfish_paymentgateway_get_session();

      $form['uc_bigfish_paymentgateway_extra']['OtpCardPocketId'] = array(
        '#type' => 'select',
        '#title' => t('Pocket identifier'),
        '#empty_option' => '',
        '#empty_value' => '',
        '#options' => $options,
        '#default_value' => isset($session['OtpCardPocketId']) ? $session['OtpCardPocketId'] : NULL,
      );
      break;

    case 'cart-process':
      if (!_uc_bigfish_paymentgateway_cart_process_validate($form_state)) {
        return $result;
      }

      $session =& _uc_bigfish_paymentgateway_get_session();
      $values = &$form_state['values']['panes']['payment']['details']['uc_bigfish_paymentgateway_extra'];

      $session['OtpCardPocketId'] = $values['OtpCardPocketId'];
      break;

    case 'cart-review':
      $session =& _uc_bigfish_paymentgateway_get_session();
      $extra = array();

      if (isset($session['OtpCardPocketId']) && isset($options[$session['OtpCardPocketId']])) {
        $extra = array(
          array(
            'title' => t('Pocket identifier'),
            'data'  => $options[$session['OtpCardPocketId']],
          ),
        );
      }

      return $extra;
  }

  return $result;
}
