<?php
/**
 * @file
 * BIG FISH Payment Gateway module / MKB Bank payment method.
 */

/**
 * MKB Bank - "Szép" card payment method callback.
 */
function uc_bigfish_paymentgateway_method_mkbszep($op, &$order, $form = NULL, &$form_state = NULL) {

  static $options;

  if (!isset($options)) {
    $options = array(
      '1111' => t('Accomodation'),
      '2222' => t('Hospitality'),
      '3333' => t('Leisure'),
    );
  }

  $result = uc_bigfish_paymentgateway_method_common($op, $order, $form, $form_state, __FUNCTION__);

  switch ($op) {
    case 'cart-details':
      $form = &$result;
      $session =& _uc_bigfish_paymentgateway_get_session();

      $form['uc_bigfish_paymentgateway_extra']['MkbSzepCafeteriaId'] = array(
        '#type' => 'select',
        '#title' => t('Pocket identifier'),
        '#empty_option' => '',
        '#empty_value' => '',
        '#options' => $options,
        '#default_value' => isset($session['MkbSzepCafeteriaId']) ? $session['MkbSzepCafeteriaId'] : NULL,
      );
      break;

    case 'cart-process':
      if (!_uc_bigfish_paymentgateway_cart_process_validate($form_state)) {
        return $result;
      }

      $session =& _uc_bigfish_paymentgateway_get_session();
      $values = &$form_state['values']['panes']['payment']['details']['uc_bigfish_paymentgateway_extra'];

      $session['MkbSzepCafeteriaId'] = $values['MkbSzepCafeteriaId'];
      break;

    case 'cart-review':
      $session =& _uc_bigfish_paymentgateway_get_session();
      $extra = array();

      if (isset($session['MkbSzepCafeteriaId']) && isset($options[$session['MkbSzepCafeteriaId']])) {
        $extra[] = array(
          'title' => t('Pocket identifier'),
          'data'  => $options[$session['MkbSzepCafeteriaId']],
        );
      }

      return $extra;
  }

  return $result;
}
