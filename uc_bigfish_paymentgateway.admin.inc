<?php
/**
 * @file
 * BIG FISH Payment Gateway module.
 */

/**
 * Order transaction admin page callback.
 *
 * @param object $order
 *   Übercart order.
 *
 * @return array
 *   Renderable array of content.
 */
function uc_bigfish_paymentgateway_order_transactions($order) {
  $query = db_select(db_escape_table(UcBigFishPaymentGateway::TABLE_NAME), 't')
    ->fields('t')
    ->condition('order_id', (int) $order->order_id, '=')
    ->orderBy('time_init', 'DESC');
  $result = $query->execute();

  $header = array(
    t('Time'),
    t('Provider'),
    t('Transaction ID'),
    t('Amount'),
    t('Result'),
    t('Message'),
  );
  $rows = array();
  foreach ($result as $row) {
    $rows[] = array(
      format_date($row->time_init, 'short'),
      $row->transaction_id,
      $row->provider_name,
      $row->payment_total . ' ' . $row->payment_currency,
      $row->payment_result_code,
      $row->payment_result_message,
    );
  }

  $build['transactions'] = array(
    '#theme'  => 'table',
    '#header' => $header,
    '#rows'   => $rows,
    '#empty'  => t('No transactions have been initialized for this order.'),
  );

  return $build;
}
